# Connect4 Client

This is a client for a Connect4 game created by BUG Studio

## Installation

The following operations are required to start the project

1. Install UnityHub by the link: https://public-cdn.cloud.unity3d.com/hub/prod/UnityHubSetup.exe

2. Install Unity 2019.2.14f1 by the link: unityhub://2019.2.14f1/49dd4e9fa428

3. Install Git 2.21.0+ by the link: https://git-scm.com/downloads

4. Install Git LFS 2.9.1+ by the link: https://git-lfs.github.com

5. Install Fork 2.24.1+ (optional) by the link: https://git-fork.com

6. Clone project by Fork/Git with link: https://gitlab.com/bug-studio/connect4-client.git

7. Add project to UnityHub (Projects/Add)

8. Click on Project in UnityHub

9. Press "Play" button for start the scene.