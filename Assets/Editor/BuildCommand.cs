﻿using UnityEditor;
using System.Linq;
using System;

public static class BuildCommand
{
    private const string BUILD_OPTIONS_ENV_VAR = "BuildOptions";

    public static string GetArgument(IEnvironmentInterface env, string name)
    {
        string[] args = env.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i].Contains(name))
            {
                return args[i + 1];
            }
        }
        return null;
    }

    public static string[] GetEnabledScenes()
    {
        return (
            from scene in EditorBuildSettings.scenes
            where scene.enabled
            where !string.IsNullOrEmpty(scene.path)
            select scene.path
        ).ToArray();
    }

    public static BuildTarget GetBuildTarget(IEnvironmentInterface env)
    {
        string buildTargetName = GetArgument(env, "customBuildTarget");
        if (buildTargetName.TryConvertToEnum(out BuildTarget target))
            return target;
        return BuildTarget.NoTarget;
    }

    public static string GetBuildPath(IEnvironmentInterface env)
    {
        string buildPath = GetArgument(env, "customBuildPath");
        if (buildPath == "")
        {
            throw new Exception("customBuildPath argument is missing");
        }
        return buildPath;
    }

    public static string GetBuildName(IEnvironmentInterface env)
    {
        string buildName = GetArgument(env, "customBuildName");
        if (buildName == "")
        {
            throw new Exception("customBuildName argument is missing");
        }
        return buildName;
    }

    public static BuildOptions GetBuildOptions(IEnvironmentInterface env)
    {
        if (TryGetEnv(env, BUILD_OPTIONS_ENV_VAR, out string envVar))
        {
            string[] allOptionVars = envVar.Split(',');
            BuildOptions allOptions = BuildOptions.None;
            string optionVar;
            int length = allOptionVars.Length;

            for (int i = 0; i < length; i++)
            {
                optionVar = allOptionVars[i];

                if (optionVar.TryConvertToEnum(out BuildOptions option))
                {
                    allOptions |= option;
                }
            }

            return allOptions;
        }

        return BuildOptions.None;
    }

    public static string GetFixedBuildPath(BuildTarget buildTarget, string buildPath, string buildName)
    {
        if (buildTarget.ToString().ToLower().Contains("windows"))
        {
            buildName += ".exe";
        }

        return buildPath + buildName;
    }

    public static bool TryConvertToEnum<TEnum>(this string strEnumValue, out TEnum value)
    {
        if (!Enum.IsDefined(typeof(TEnum), strEnumValue))
        {
            value = default;
            return false;
        }

        value = (TEnum) Enum.Parse(typeof(TEnum), strEnumValue);
        return true;
    }

    public static bool TryGetEnv(IEnvironmentInterface env, string key, out string value)
    {
        value = env.GetEnvironmentVariable(key);
        return !string.IsNullOrEmpty(value);
    }

    public static void PerformBuild()
    {
        IEnvironmentInterface env = new CustomEnvironment();
        var buildTarget    = GetBuildTarget(env);
        var buildPath      = GetBuildPath(env);
        var buildName      = GetBuildName(env);
        var buildOptions   = GetBuildOptions(env);
        var fixedBuildPath = GetFixedBuildPath(buildTarget, buildPath, buildName);

        BuildPipeline.BuildPlayer(GetEnabledScenes(), fixedBuildPath, buildTarget, buildOptions);
    }
   
}

public interface IEnvironmentInterface
{
    string[] GetCommandLineArgs();
    string GetEnvironmentVariable(string key);
}

public class CustomEnvironment : IEnvironmentInterface
{
    public string[] GetCommandLineArgs()
    {
        return Environment.GetCommandLineArgs();
    }

    public string GetEnvironmentVariable(string key)
    {
        return Environment.GetEnvironmentVariable(key);
    }
}
