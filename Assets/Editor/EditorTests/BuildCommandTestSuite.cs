﻿using NUnit.Framework;
using Moq;
using System;
using UnityEditor;

[TestFixture]
public class BuildCommandTestSuite
{
    readonly Mock<IEnvironmentInterface> mockedEnv = new Mock<IEnvironmentInterface>();
    readonly string[] mockedArgs = new[] {
            "projectPath", "/project/path",
            "quit",
            "batchmode",
            "buildTarget", "StandaloneWindows64",
            "customBuildTarget", "StandaloneWindows64",
            "customBuildName", "Connect4Client",
            "customBuildPath", "./Builds/StandaloneWindows64/",
            "executeMethod", "BuildCommand.PerformBuild",
            "logFile", "/dev/stdout"
        };

    private const string BUILD_OPTIONS_ENV_VAR = "BuildOptions";

    public IEnvironmentInterface applyReturnMockeArgs()
    {
        mockedEnv.Setup(m => m.GetCommandLineArgs()).Returns(mockedArgs);
        return mockedEnv.Object;
    }

    [Test]
    public void GetArgument()
    {
        var env = applyReturnMockeArgs();

        var args = env.GetCommandLineArgs();
        Assert.AreEqual(args, mockedArgs);

        string argument = BuildCommand.GetArgument(env, "buildTarget");
        Assert.AreEqual(argument, "StandaloneWindows64");

        argument = BuildCommand.GetArgument(env, "testBuildTarget");
        Assert.IsNull(argument);
    }

    [Test]
    public void GetBuildTarget()
    {
        var env = applyReturnMockeArgs();

        BuildTarget buildTarget = BuildCommand.GetBuildTarget(env);
        Assert.IsInstanceOf<BuildTarget>(buildTarget);
        Assert.AreEqual("StandaloneWindows64", buildTarget.ToString());

        string[] testArgs = mockedArgs;
        testArgs.SetValue("", 7);
        mockedEnv.Setup(m => m.GetCommandLineArgs()).Returns(testArgs);

        buildTarget = BuildCommand.GetBuildTarget(env);
        Assert.IsInstanceOf<BuildTarget>(buildTarget);
        Assert.AreEqual(BuildTarget.NoTarget, buildTarget);

        testArgs.SetValue("someValue", 7);
        mockedEnv.Setup(m => m.GetCommandLineArgs()).Returns(testArgs);

        buildTarget = BuildCommand.GetBuildTarget(env);
        Assert.IsInstanceOf<BuildTarget>(buildTarget);
        Assert.AreEqual(BuildTarget.NoTarget, buildTarget);
    }

    [Test]
    public void GetBuildPath()
    {
        var env = applyReturnMockeArgs();

        string buildPath = BuildCommand.GetBuildPath(env);
        Assert.AreEqual(buildPath, "./Builds/StandaloneWindows64/");

        string[] testArgs = mockedArgs;
        testArgs.SetValue("", 11);
        mockedEnv.Setup(m => m.GetCommandLineArgs()).Returns(testArgs);

        Assert.Throws<Exception>(delegate { BuildCommand.GetBuildPath(env); });
    }

    [Test]
    public void GetBuildName()
    {
        var env = applyReturnMockeArgs();

        string buildName = BuildCommand.GetBuildName(env);
        Assert.AreEqual(buildName, "Connect4Client");

        string[] testArgs = mockedArgs;
        testArgs.SetValue("", 9);
        mockedEnv.Setup(m => m.GetCommandLineArgs()).Returns(testArgs);

        Assert.Throws<Exception>(delegate { BuildCommand.GetBuildName(env); });
    }

    [Test]
    public void GetBuildOptions()
    {
        mockedEnv.Setup(m => m.GetCommandLineArgs()).Returns(mockedArgs);
        mockedEnv.Setup(m => m.GetEnvironmentVariable(BUILD_OPTIONS_ENV_VAR)).Returns("Development,StrictMode");
        var env = mockedEnv.Object;

        BuildOptions testOptions = BuildOptions.None;
        testOptions |= BuildOptions.Development;
        testOptions |= BuildOptions.StrictMode;

        BuildOptions buildOptions = BuildCommand.GetBuildOptions(env);
        Assert.IsInstanceOf<BuildOptions>(buildOptions);
        Assert.AreEqual(buildOptions, testOptions);
        Assert.AreNotEqual(buildOptions, testOptions |= BuildOptions.Reserved1);

        mockedEnv.Setup(m => m.GetEnvironmentVariable(BUILD_OPTIONS_ENV_VAR)).Returns("");

        buildOptions = BuildCommand.GetBuildOptions(env);
        Assert.IsInstanceOf<BuildOptions>(buildOptions);
        Assert.AreEqual(buildOptions, BuildOptions.None);
    }

    [Test]
    public void GetFixedBuildPath()
    {
        BuildTarget buildTarget = (BuildTarget)Enum.Parse(typeof(BuildTarget), "StandaloneWindows64");

        string fixedBuildPath = BuildCommand.GetFixedBuildPath(buildTarget, "./Builds/StandaloneWindows64/", "Connect4Client");
        Assert.AreEqual(fixedBuildPath, "./Builds/StandaloneWindows64/Connect4Client.exe");

        buildTarget = (BuildTarget)Enum.Parse(typeof(BuildTarget), "StandaloneLinux64");

        fixedBuildPath = BuildCommand.GetFixedBuildPath(buildTarget, "./Builds/StandaloneLinux64/", "Connect4Client");
        Assert.AreEqual(fixedBuildPath, "./Builds/StandaloneLinux64/Connect4Client");
    }

    [Test]
    public void TryConvertToEnum()
    {
        Assert.IsTrue("StandaloneWindows64".TryConvertToEnum(out BuildTarget target));
        Assert.IsInstanceOf<BuildTarget>(target);
        Assert.AreEqual("StandaloneWindows64", target.ToString());

        Assert.IsFalse("SomeLinux".TryConvertToEnum(out BuildTarget target2));
        Assert.IsInstanceOf<BuildTarget>(target2);
        Assert.IsTrue(target2 == 0);
    }

    [Test]
    public void TryGetEnv()
    {
        mockedEnv.Setup(m => m.GetEnvironmentVariable(BUILD_OPTIONS_ENV_VAR)).Returns("IncludeTestAssemblies,StrictMode");
        var env = mockedEnv.Object;

        Assert.IsTrue(BuildCommand.TryGetEnv(env, BUILD_OPTIONS_ENV_VAR, out string envVar));
        Assert.AreEqual(envVar, "IncludeTestAssemblies,StrictMode");

        mockedEnv.Setup(m => m.GetEnvironmentVariable(BUILD_OPTIONS_ENV_VAR)).Returns("");

        Assert.IsFalse(BuildCommand.TryGetEnv(env, BUILD_OPTIONS_ENV_VAR, out string envVar2));
        Assert.AreEqual(envVar2, "");
    }
}